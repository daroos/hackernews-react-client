export default class Api {
  public static getTopNews() {
    return fetch(`https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`)
      .then(response => response.json())
      .then(topNews => topNews)
      .catch(error => {
        console.log('error fetching top news list');
      });
  }

  public static getItemById(id: string) {
    return fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
      .then(response => response.json())
      .then(topNewsItem => topNewsItem)
      .catch(error => {
        console.log(`error fetching top news item with id ${id}`);
      });
  }
}