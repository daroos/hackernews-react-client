export interface Comment {
  by: string;
  id: number;
  kids?: number[];
  parent: number;
  text: string;
  time: string;
  type: string;
  deleted?: boolean;
}