import * as React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import { Menu } from './components/Menu/Menu';
import { News } from './components/News/News';
import { Newest } from './components/Newest/Newest';
import { Newcomments } from './components/Newcomments/Newcomments';
import { Show } from './components/Show/Show';
import { Ask } from './components/Ask/Ask';
import { Jobs } from './components/Jobs/Jobs';
import { Submit } from './components/Submit/Submit';
import { TopNewsContainer } from './components/TopNewsContainer/TopNewsContainer';
import { ItemWithComments } from './components/ItemWithComments/ItemWithComments';

import './App.css';

class App extends React.Component {
  public render() {
    return (
      <Router>
        <div className='container'>
          <Menu />

          <div className='main-content'>
            <Switch>
              <Route path='/news' component={News} />
              <Route path='/newest' component={Newest} />
              <Route path='/newcomments' component={Newcomments} />
              <Route path='/show' component={Show} />
              <Route path='/ask' component={Ask} />
              <Route path='/jobs' component={Jobs} />
              <Route path='/submit' component={Submit} />
              <Route path='/item/:itemId' component={ItemWithComments} />
              <Route path='/' component={TopNewsContainer} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
