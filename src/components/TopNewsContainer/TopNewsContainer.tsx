import * as React from 'react';
import * as QueryString from 'querystring';
import { History, Location } from 'history';
import { match } from 'react-router';

import Api from '../../rest-api/api';
import { TopNewsList } from './TopNewsList/TopNewsList';
import { MoreLink } from '../MoreLink/MoreLink';

interface Props {
  history: History;
  location: Location;
  match: match;
}

interface State {
  topNews: string[];
}

export class TopNewsContainer extends React.Component<Props, State> {

  state: State = {
    topNews: []
  };

  public componentDidMount(): void {
    Api.getTopNews()
      .then(topNews => this.setState({ topNews }));
  }

  private getPageNumberFromQuery() {
    const { search } = this.props.location;

    const pageNumber = +QueryString.parse(search.slice(1)).page;

    return pageNumber || 0;
  }

  private changePage = () => {
    this.props.history.push(
      `${this.props.history.location.pathname}?page=${this.getPageNumberFromQuery()+1}`
    );
  };

  private getNewsForThePage() {
    const pageNumber: number = this.getPageNumberFromQuery();

    if (pageNumber) {
      const start = pageNumber * 30;
      const end = pageNumber * 30 + 30;

      return [...this.state.topNews].slice(start, end);
    }

    return [...this.state.topNews].slice(0, 30);
  }

  public render() {
    return (
      <React.Fragment>
        <TopNewsList topNews={this.getNewsForThePage()} />
        <MoreLink onClick={this.changePage} />
      </React.Fragment>
    );
  }
}