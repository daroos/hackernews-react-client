import * as React from 'react';

import { TopNewsListItem } from './TopNewsListItem/TopNewsListItem';

interface Props {
  topNews: string[]
}

export const TopNewsList = (props: Props) => {
  console.log(props);
  return (<div>
    {props.topNews.map(topNewsItemId => (
      <TopNewsListItem topNewsItemId={topNewsItemId} key={topNewsItemId} />
    ))}
  </div>)
};