import * as React from 'react';
import { Link } from 'react-router-dom';

import Api from '../../../../rest-api/api';
import { TopNewsItem } from '../../../../models/TopNewsItem';
import { extractRootDomain } from '../../../../helpers/domain-parser';

import './TopNewsListItem.css';

interface Props {
  topNewsItemId: string
}

interface State {
  topNewsItem?: TopNewsItem
}

export class TopNewsListItem extends React.Component<Props, State> {
  state: State = {
    topNewsItem: undefined
  };

  public componentDidMount() {
    Api.getItemById(this.props.topNewsItemId)
      .then(topNewsItem => this.setState({ topNewsItem }));
  }

  private renderCommentsCount() {
    if (!this.state.topNewsItem) {
      return null;
    }

    const { kids, id } = this.state.topNewsItem;

    return kids ? <Link to={`/item/${id}`}> | {kids.length} comments</Link> : null;
  }

  render() {
    if (!this.state.topNewsItem) {
      return <div>Loading...</div>;
    }

    const { title, url, score, by, time } = this.state.topNewsItem;
    const formattedDate = new Date(+time * 1000).toLocaleString();

    return (
      <div className='list-item'>
        <div className='title'><a href={url} rel="noopener noreferrer" target="_blank">{title}</a> <span className='subtext'>{url && `(${extractRootDomain(url)})`}</span></div>
        <div className='subtext'>{score} points by {by} | {formattedDate} {this.renderCommentsCount()}</div>
      </div>
    )
  }
}
