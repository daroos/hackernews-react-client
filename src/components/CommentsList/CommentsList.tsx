import * as React from 'react';

import { CommentsListItemContainer } from './CommentsListItemContainer/CommentsListItemContainer';

interface Props {
  commentIds: number[];
}

export class CommentsList extends React.Component<Props> {
  render() {
    return (
      <div>
        {this.props.commentIds.map(commentId => (
          <CommentsListItemContainer commentId={commentId} key={commentId} />
        ))}
      </div>
    );
  }
}