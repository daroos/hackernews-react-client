import * as React from 'react';
import { mount } from 'enzyme';

import { CommentsListItem } from './CommentsListItem';

describe('<CommentsListItem />', () => {
  it('should render "Loading..." when comment not provided', () => {
    const wrapper = mount(<CommentsListItem />);

    expect(wrapper.text()).toContain('Loading...');
  });

  it('should render "by" and "text" properly', () => {
    const commentProps = {
      by: 'User',
      id: 12345,
      parent: 1234,
      text: 'Comment text',
      time: '1539021561',
      type: 'comment',
      deleted: false,
    };

    const wrapper = mount(<CommentsListItem comment={commentProps} />);

    expect(wrapper.text()).toContain('User');
    expect(wrapper.text()).toContain('Comment text');
  });
});