import * as React from 'react';

import { CommentsList } from '../../CommentsList';
import { Comment } from '../../../../models/Comment';

import './CommentsListItem.css';

interface Props {
  comment?: Comment;
}

export const CommentsListItem = (props: Props) => {
  if (!props.comment) {
    return <div>Loading...</div>;
  }

  if (props.comment.deleted) {
    return null;
  }

  const { time, by, kids, text } = props.comment;

  const formattedDate = new Date(+time * 1000).toLocaleString();

  return (
    <div className='comment'>
      <div className='subtext'>{by} | {formattedDate}</div>
      <div className='text' dangerouslySetInnerHTML={{__html: text}} />

      {kids ? <CommentsList commentIds={kids} /> : null}
    </div>
  );
};
