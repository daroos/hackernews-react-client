import * as React from 'react';

import Api from '../../../rest-api/api';
import { Comment } from '../../../models/Comment';
import { CommentsListItem } from './CommentsListItem/CommentsListItem';

interface Props {
  commentId: number;
}

interface State {
  comment?: Comment;
}

export class CommentsListItemContainer extends React.Component<Props, State> {
  state: State = {
    comment: undefined,
  };

  public componentDidMount() {
    Api.getItemById(`${this.props.commentId}`)
      .then(comment => this.setState({ comment }));
  }

  public render() {
    return (
      <CommentsListItem comment={this.state.comment} />
    );
  }
}