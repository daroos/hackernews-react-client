import * as React from 'react';

import Api from '../../rest-api/api';
import { TopNewsItem } from '../../models/TopNewsItem';
import { RouteComponentProps } from 'react-router';
import { TopNewsListItem } from '../TopNewsContainer/TopNewsList/TopNewsListItem/TopNewsListItem';
import { CommentsList } from '../CommentsList/CommentsList';

interface RouteProps {
  itemId: string;
}

interface Props extends RouteComponentProps<RouteProps> {

}

interface State {
  topNewsItem?: TopNewsItem;
}

export class ItemWithComments extends React.Component<Props, State> {
  state: State = {
    topNewsItem: undefined
  };

  public componentDidMount() {
    Api.getItemById(this.props.match.params.itemId)
      .then(topNewsItem => this.setState({ topNewsItem }));
  }

  renderTopNewsListItem(): React.ReactElement<TopNewsListItem> {
    return <TopNewsListItem topNewsItemId={this.props.match.params.itemId} />;
  }

  renderCommentsList() {
    return this.state.topNewsItem && this.state.topNewsItem.kids ? <CommentsList commentIds={this.state.topNewsItem.kids} /> : 'Loading...';
  }

  render() {
    return (
      <div>
        {this.renderTopNewsListItem()}
        {this.renderCommentsList()}
      </div>
    );
  }
}