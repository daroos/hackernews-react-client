import * as React from 'react';

interface Props {
  onClick(): void
}

export const MoreLink = (props: Props) => (
  <a onClick={props.onClick}>
    more...
  </a>
);