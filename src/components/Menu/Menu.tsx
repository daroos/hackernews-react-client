import * as React from 'react';
import { Link } from 'react-router-dom';

import './Menu.css';

export const Menu = () => (
  <div>
    <div className="main-menu">
      <Link to='/'>Logo</Link>{' '}
      <Link to='/news'>Hacker News</Link>{' '}
      <Link to='/newest'>new</Link>{' | '}
      <Link to='/newcomments'>comments</Link>{' | '}
      <Link to='/show'>show</Link>{' | '}
      <Link to='/ask'>ask</Link>{' | '}
      <Link to='/jobs'>jobs</Link>{' | '}
      <Link to='/submit'>submit</Link>
    </div>
  </div>
);